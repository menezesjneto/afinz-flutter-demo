import 'package:date_format/date_format.dart';

class Extractor {
  static final Extractor _singleton = new Extractor._internal();
  factory Extractor() {
    return _singleton;
  }
  Extractor._internal();

  static String extractString(value, String defaultValue) {
    return (value!=null && value!='')?value.toString():defaultValue;
  }

  static double extractDouble(value) {
    try {
      return value!=null?double.parse(value.toString()):0.0;
    } catch (e) {
      return 0.0;
    }
  }

  static bool extractBool(value) {
    return value==true?true:false;
  }

  static DateTime extractDate(value){
    try {
      if(value is String) return value!=null?DateTime.parse(value):null;
      else return value!=null?value.toDate():null;
    } catch (e) {
      return null;
    }
  }

  static String extractDateII(value){
    try {
      if(value != null) return formatDate(value, [yyyy, '-', mm, '-', dd, 'T', HH, ':', nn, ':', ss])+'Z';
      else return null;
    } catch (e) {
      return null;
    }
  }

  static String extractDateIII(value){
    try {
      if(value != null) return formatDate(value, [yyyy, '-', mm, '-', dd]);
      else return null;
    } catch (e) {
      return null;
    }
  }

  static int extractInt(value){
    try {
      if(value is String){
        var val = value.substring(0, value.length-2);
        return int.parse(val);
      }
      else if(value is double){
        return value.toInt();
      }
      else return value!=null?int.parse(value.toString()):0;
    } catch (e) {
      return 0;
    }
  }

  static String extractCpfCnpj(value){
    try {
      if(value != null) return value.replaceAll('.', '').replaceAll('-', '').replaceAll('/', '');
      else return null;
    } catch (e) {
      return null;
    }
  }





  static List<dynamic> toArrayStringsFromDocRef(array){
    if(array != null) {
      List<dynamic> list = [];
      for (var item in array) {
        list.add(item.path);
      }
      
      return list;
    }
    else return [];
  }


}
