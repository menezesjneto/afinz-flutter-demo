import 'dart:ui';
import 'package:afinz/models/login_data.dart';
import 'package:afinz/pages/oferta/ler_oferta_page.dart';
import 'package:afinz/providers/api_provider.dart';
import 'package:afinz/services/page_transition.dart';
import 'package:afinz/utils/customs_colors.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  State createState() => new LoginPageState();
}

class LoginPageState extends State<LoginPage> with SingleTickerProviderStateMixin {

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _scaffoldKey2 = GlobalKey<ScaffoldState>();

  TextEditingController emailRedefinicaoController = new TextEditingController();

  final FocusNode _loginFocus = FocusNode();
  final FocusNode _senhaFocus = FocusNode();
  final FocusNode _emailRedefinicaoFocus = FocusNode();

  bool emailRedefinicaoEnviado =false;

  bool sending = false;
  bool sendingRedefinicao = false;
  bool obscureText = true;

  LoginData _loginData = new LoginData(
    email: '',
    senha: ''
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: SafeArea(
          child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: SingleChildScrollView( child: Container(
                alignment: Alignment.center,
                height: MediaQuery.of(context).size.height, 
                child: new BackdropFilter(
                filter: new ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                child:  Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 20.0),
                      child: Form(
                        key: _formKey,
                        autovalidate: true,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[

                              Container(
                                margin: EdgeInsets.only(bottom: 30),
                                width: 150,
                                height: 150,
                                child: Image.asset("assets/images/logo2.png",fit: BoxFit.contain,),
                              ),
                          
                          
                              //EMAIL
                              Container(
                                color: Colors.grey[100],
                                margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 20.0),
                                padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 0.0),
                                child: ListTile(
                                  contentPadding: EdgeInsets.all(0.0),
                                  subtitle: TextFormField(
                                    initialValue: _loginData.email,
                                    style: TextStyle(color: Colors.black54, fontSize: 16.0),
                                    decoration: InputDecoration(
                                      hintText: "Email",
                                      hintStyle: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w400
                                      ),
                                      prefixIcon: Icon(Icons.mail, color: Colors.black26),
                                      suffixIcon: ApiProvider.validEmail(_loginData.email)?Icon(Icons.check_circle, size: 20.0, color: CustomsColors.primaryColor,):null,
                                    ),
                                    keyboardType: TextInputType.emailAddress,
                                    textCapitalization: TextCapitalization.none,
                                    textInputAction: TextInputAction.next,
                                    cursorColor: Colors.black,
                                    focusNode: _loginFocus,
                                    onFieldSubmitted: (term){
                                      _loginFocus.unfocus();
                                      FocusScope.of(context).requestFocus(_senhaFocus);
                                    },
                                    onChanged: (val) =>
                                      setState(() => _loginData.email = val.trim()),
                                  ),
                                ),
                              ),
                                    

                              //SENHA
                              Container(
                                color: Colors.grey[100],
                                margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                                padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 0.0),
                                child: ListTile(
                                  contentPadding: EdgeInsets.all(0.0),
                                  subtitle: TextFormField(
                                    initialValue: _loginData.senha,
                                    style: TextStyle(color: Colors.black54, fontSize: 16.0),
                                    decoration: InputDecoration(
                                      hintText: "Senha",
                                      hintStyle: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w400
                                      ),
                                      prefixIcon: Icon(Icons.lock, color: Colors.black26,),
                                      suffixIcon: IconButton(
                                        icon: Icon(obscureText?FontAwesomeIcons.solidEye:FontAwesomeIcons.solidEyeSlash, color: CustomsColors.primaryColor, size: 20.0), 
                                        onPressed: (){
                                          setState(() {
                                            obscureText = !obscureText;
                                          });
                                        }
                                      )
                                    ),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.none,
                                    textInputAction: TextInputAction.done,
                                    obscureText: obscureText,
                                    cursorColor: Colors.black,
                                    focusNode: _senhaFocus,
                                    onFieldSubmitted: (term){
                                      _senhaFocus.unfocus();
                                    },
                                    onChanged: (val) =>
                                      setState(() => _loginData.senha = val.trim()),
                                  ),
                                ),
                              ),

                        
                              Container(
                                margin: EdgeInsets.all(20.0),
                                width: MediaQuery.of(context).size.width * 0.8,
                                height: 60.0,
                                child: RaisedButton(
                                  color: CustomsColors.primaryColor,
                                  disabledColor: Colors.grey[500],
                                  child: Text('Entrar', style: TextStyle(color: Colors.white, fontSize: 17.0),),
                                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                                  onPressed: () {
                                    
                                    _loginFocus.unfocus();
                                    _senhaFocus.unfocus();

                                    
                                  }, 
                                )
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 20.0, bottom: 20),
                                alignment: Alignment.center,
                                child: GestureDetector(
                                  child: Text('Esqueci a senha', style: TextStyle(color: CustomsColors.primaryColor, fontSize: 17.0, fontWeight: FontWeight.w500)),
                                  onTap: () {
                                    _containerForSheetEsqueciSenha();
                                  },
                                ),
                              ),


                              
                              Container(
                                margin: EdgeInsets.only(top: 40.0, bottom: 10),
                                child: Text(
                                  "Ainda não tem um cartão Afinz?",
                                  style: TextStyle(fontSize: 16.0, color: Colors.black87, fontWeight: FontWeight.bold),
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.all(20.0),
                                width: MediaQuery.of(context).size.width * 0.8,
                                height: 60.0,
                                child: RaisedButton(
                                  color: CustomsColors.primaryColor,
                                  disabledColor: Colors.grey[500],
                                  child: sending ? CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.white)) : Text('Pedir cartão', style: TextStyle(color: Colors.white, fontSize: 17.0),),
                                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                                  onPressed: () {
                                    
                                    _loginFocus.unfocus();
                                    _senhaFocus.unfocus();

                                    _login();
                                  }, 
                                )
                              ),
                                 
                            ],
                                
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(20.0)
                        ),
                      ),
                      Container(height: 70,)
                  
                  ]
                )),
              )
            )
          )
        ),
        bottomNavigationBar: Container(height: 0.0),
      )
    );
  }

  void _login(){
    setState(() {
      sending = true;
    });
    Future.delayed(Duration(seconds: 2)).then((value){
      setState(() {
        sending = false;
      });
      Navigator.push(context, SlideRightRoute(page: LerOfertaPage()));
    });
  }


_containerForSheetEsqueciSenha(){
  showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext ctx) {
      return StatefulBuilder(builder: (context, StateSetter setState2) {
  
        return Container(
          alignment: Alignment.center,
          margin: EdgeInsets.fromLTRB(20, 60, 20, 60),
          child: Scaffold(
          key: _scaffoldKey2,
          body: Container(
            alignment: Alignment.center,
            height: MediaQuery.of(context).size.height,
            child: Material(
            borderOnForeground: true,
            color: Colors.transparent,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: SingleChildScrollView(child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                  Text("Esqueci minha senha", style: TextStyle(fontSize: 18, color: Colors.black87, fontWeight: FontWeight.w300), textAlign: TextAlign.center),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                    Container(
                      width: 80,
                      height: 80,
                      child: FlareActor('assets/email_flip.flr', animation: 'Jump')
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.green.withOpacity(0.1),
                        borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                      alignment: Alignment.center,
                      width: 250,
                      margin: EdgeInsets.only(top: 20),
                      height: 80,
                      child: Text('Enviaremos para você um email com instruções de redefinição de senha', 
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 14, color: Colors.black87, fontWeight: FontWeight.w300)
                      ,),
                    ),
                      
                    ],
                  ),
                  Divider(
                    color: Colors.transparent,
                  ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 20.0),
                          padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 0.0),
                          child: ListTile(
                            contentPadding: EdgeInsets.all(0.0),
                            subtitle: TextFormField(
                              controller: emailRedefinicaoController,
                              style: TextStyle(color: Colors.black54, fontSize: 16.0),
                              decoration: InputDecoration(
                                hintText: "Email",
                                hintStyle: TextStyle(
                                  color: CustomsColors.primaryColor,
                                  fontSize: 16.0,
                                ),
                                prefixIcon: Icon(Icons.mail, color: CustomsColors.primaryColor),
                              ),
                              keyboardType: TextInputType.emailAddress,
                              textCapitalization: TextCapitalization.none,
                              textInputAction: TextInputAction.next,
                              cursorColor: Colors.black,
                              focusNode: _emailRedefinicaoFocus,
                              onFieldSubmitted: (term){
                                _emailRedefinicaoFocus.unfocus();
                              },
                            ),
                          ),
                        ),

                        Container(
                          child: Container(
                            height: 60,
                            margin: EdgeInsets.all(15),
                              width: MediaQuery.of(context).size.width*0.6,
                              decoration: BoxDecoration(
                                color: CustomsColors.primaryColor,
                                borderRadius: BorderRadius.all(Radius.circular(8.0))
                              ),
                              child: OutlineButton(
                              splashColor: CustomsColors.primaryColor,
                              color: emailRedefinicaoEnviado ? Colors.blue : CustomsColors.primaryColor,
                              borderSide: BorderSide.none,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
                              highlightElevation: 0,
                            //  borderSide: BorderSide(color: Colors.grey),
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: sendingRedefinicao == true ? CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.white)) : emailRedefinicaoEnviado ? Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(
                                        'Email enviado!',
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.white, fontWeight: FontWeight.w300
                                        ),
                                    ),
                                    Icon(FontAwesomeIcons.check, color: Colors.white),
                                  ],
                                ) : Text(
                                  'Enviar',
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white, fontWeight: FontWeight.w300
                                  )
                                ),
                              ),
                            onPressed: emailRedefinicaoEnviado ? (){} : (){
                              if( ApiProvider.validEmail(emailRedefinicaoController.text)) _redefinirSenha(setState2);
                            },
                            ),
                            ),
                        ),

                        Container(
                          child: Container(
                            height: 60,
                            margin: EdgeInsets.all(15),
                              width: MediaQuery.of(context).size.width*0.6,
                              decoration: BoxDecoration(
                                //color: CustomsColors.primaryColor,
                                borderRadius: BorderRadius.all(Radius.circular(8.0))
                              ),
                              child: OutlineButton(
                              splashColor: CustomsColors.primaryColor,
                              color: CustomsColors.primaryColor,
                              highlightedBorderColor: CustomsColors.primaryColor,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
                              highlightElevation: 0,
                              borderSide: BorderSide(color: CustomsColors.primaryColor),
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Icon(LineAwesomeIcons.long_arrow_left, color: CustomsColors.primaryColor),
                                    Text(
                                      'Cancelar',
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: CustomsColors.primaryColor, fontWeight: FontWeight.w300
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            onPressed: (){
                              setState2(() {
                                Navigator.pop(context);
                                emailRedefinicaoController.text = "";
                                sendingRedefinicao = false;
                                emailRedefinicaoEnviado  = false;
                              });
                            },
                            ),
                            ),
                        ),
                      ],
                    )
                  ]),
            ),
          ),
        )));
      });
      },
    );
  }

_redefinirSenha(setState2){
  setState2(() {
    sendingRedefinicao = true;
  });
}

}
