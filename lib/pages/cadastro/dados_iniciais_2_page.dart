import 'dart:ui';
import 'package:afinz/models/cadastro.dart';
import 'package:afinz/pages/cadastro/negativa_credito_page.dart';
import 'package:afinz/services/page_transition.dart';
import 'package:afinz/utils/customs_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class DadosIniciais2Page extends StatefulWidget {
  Cadastro cadastro;
  DadosIniciais2Page({Key key, this.cadastro}) : super(key: key);
  
  @override
  State createState() => new DadosIniciais2PageState();
}

class DadosIniciais2PageState extends State<DadosIniciais2Page> with SingleTickerProviderStateMixin {

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController emailRedefinicaoController = new TextEditingController();
  bool sending = false;
  bool sendingRedefinicao = false;

  var maskFormatterCPF = new MaskTextInputFormatter(mask: '###.###.###-##', filter: { "#": RegExp(r'[0-9]') });
  var maskFormatterTelefone = new MaskTextInputFormatter(mask: '(##) #####-####', filter: { "#": RegExp(r'[0-9]') });

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        Navigator.pop(context);
      },
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          centerTitle: false,
          leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined, color: Colors.black54),
            onPressed: (){
                Navigator.pop(context);
            },
          ),
        ),
        body: SafeArea(
          child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: SingleChildScrollView( child: Container(
              alignment: Alignment.topCenter,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.all(20),
                      child: Text(
                        "Para começar",
                        style: TextStyle(fontSize: 20.0, color: CustomsColors.primaryColor, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),

                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(left: 20, bottom: 20),
                      child: Text(
                        "Precisamos te conhecer um pouco melhor",
                        style: TextStyle(fontSize: 17.0, color: Colors.black87, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.start,
                      ),
                    ),

                    
                    //CPF
                    Container(
                      color: Colors.grey[100],
                      margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                      padding: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 0.0),
                      child: ListTile(
                        contentPadding: EdgeInsets.all(0.0),
                        subtitle: TextFormField(
                          enabled: false,
                          initialValue: widget.cadastro.cpf,
                          inputFormatters: [maskFormatterCPF],
                          style: TextStyle(color: Colors.black54, fontSize: 16.0),
                          decoration: InputDecoration(
                            hintText: "CPF",
                            hintStyle: TextStyle(
                              color: Colors.black54,
                              fontSize: 17.0,
                              fontWeight: FontWeight.w400
                            ),
                            suffixIcon: Icon(Icons.close, color: Colors.grey[400],)
                          ),
                          keyboardType: TextInputType.number,
                          textCapitalization: TextCapitalization.none,
                          textInputAction: TextInputAction.done,
                          cursorColor: Colors.black,
                        ),
                      ),
                    ),

                    //NOME
                    Container(
                      color: Colors.grey[100],
                      margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                      padding: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 0.0),
                      child: ListTile(
                        contentPadding: EdgeInsets.all(0.0),
                        subtitle: TextFormField(
                          enabled: false,
                        initialValue: widget.cadastro.nome,
                          style: TextStyle(color: Colors.black54, fontSize: 16.0),
                          decoration: InputDecoration(
                            suffixIcon: Icon(Icons.close, color: Colors.grey[400],),
                            hintText: "Nome completo",
                            hintStyle: TextStyle(
                              color: Colors.black54,
                              fontSize: 17.0,
                              fontWeight: FontWeight.w400
                            ),
                          ),
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.none,
                          textInputAction: TextInputAction.done,
                          cursorColor: Colors.black,
                        ),
                      ),
                    ),

                    //EMAIL
                    Container(
                      color: Colors.grey[100],
                      margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                      padding: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 0.0),
                      child: ListTile(
                        contentPadding: EdgeInsets.all(0.0),
                        subtitle: TextFormField(
                          enabled: false,
                          initialValue: widget.cadastro.email,
                          style: TextStyle(color: Colors.black54, fontSize: 16.0),
                          decoration: InputDecoration(
                            suffixIcon: Icon(Icons.close, color: Colors.grey[400],),
                            hintText: "Email",
                            hintStyle: TextStyle(
                              color: Colors.black54,
                              fontSize: 17.0,
                              fontWeight: FontWeight.w400
                            ),
                          ),
                          keyboardType: TextInputType.emailAddress,
                          textCapitalization: TextCapitalization.none,
                          textInputAction: TextInputAction.done,
                          cursorColor: Colors.black,
                        ),
                      ),
                    ),

                    //CELULAR
                    Container(
                      color: Colors.grey[100],
                      margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                      padding: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 0.0),
                      child: ListTile(
                        contentPadding: EdgeInsets.all(0.0),
                        subtitle: TextFormField(
                          enabled: false,
                          initialValue: widget.cadastro.telefone,
                          inputFormatters: [maskFormatterTelefone],
                          style: TextStyle(color: Colors.black54, fontSize: 16.0),
                          decoration: InputDecoration(
                            suffixIcon: Icon(Icons.close, color: Colors.grey[400],),
                            hintText: "Celular com DD",
                            hintStyle: TextStyle(
                              color: Colors.black54,
                              fontSize: 17.0,
                              fontWeight: FontWeight.w400
                            ),
                          ),
                          keyboardType: TextInputType.phone,
                          textCapitalization: TextCapitalization.none,
                          textInputAction: TextInputAction.done,
                          cursorColor: Colors.black,
                        ),
                      ),
                    ),
                  ],
                )
              )
            )
          )
        ),
        bottomNavigationBar: Container(
          height: MediaQuery.of(context).size.height * 0.2,
          child: Column(
            children: [
            
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                width: MediaQuery.of(context).size.width * 0.8,
                height: 60.0,
                child: RaisedButton(
                  elevation: 0,
                  color: CustomsColors.primaryColor,
                  disabledColor: Colors.grey[500],
                  child: Text('Tudo certo', style: TextStyle(color: Colors.white, fontSize: 17.0),),
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                  onPressed: (){
                    Navigator.push(context, SlideRightRoute(page: NegativaCreditoPage()));
                  }, 
                )
              ),
               Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                width: MediaQuery.of(context).size.width * 0.8,
                height: 60.0,
                child: Column(
                  children: [
                    Text('Ao continuar você está de acordo', style: TextStyle(color: Colors.grey[400], fontSize: 17.0),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('com a ', style: TextStyle(color: Colors.grey[400], fontSize: 17.0),),
                        Text('Política de Privacidade', style: TextStyle(color: CustomsColors.primaryColor, fontSize: 17.0, fontWeight: FontWeight.w500),),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      )
    );
  }

}
