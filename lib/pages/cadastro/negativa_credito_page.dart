import 'dart:ui';
import 'package:afinz/models/cadastro.dart';
import 'package:afinz/pages/home_page.dart';
import 'package:afinz/services/page_transition.dart';
import 'package:afinz/utils/customs_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class NegativaCreditoPage extends StatefulWidget {
  Cadastro cadastro;
  NegativaCreditoPage({Key key, this.cadastro}) : super(key: key);
  
  @override
  State createState() => new NegativaCreditoPageState();
}

class NegativaCreditoPageState extends State<NegativaCreditoPage> with SingleTickerProviderStateMixin {

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController emailRedefinicaoController = new TextEditingController();
  bool sending = false;
  bool sendingRedefinicao = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        Navigator.pop(context);
      },
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          centerTitle: false,
          leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined, color: Colors.black54),
            onPressed: (){
                Navigator.pop(context);
            },
          ),
        ),
        body: SafeArea(
          child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: SingleChildScrollView( child: Container(
              alignment: Alignment.topCenter,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.all(20),
                      child: Icon(FontAwesomeIcons.frown, size: 200),
                    ),

                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(bottom: 20, top: 50),
                      child: Text(
                        "Fizemos uma análise de\ncrédito e nesse momento não conseguiremos efetuar o\npedido do seu cartão",
                        style: TextStyle(fontSize: 22.0, color: Colors.black87, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),

                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.all(20),
                      child: Text(
                        "Tente novamente daqui 3 meses",
                        style: TextStyle(fontSize: 20.0, color: CustomsColors.primaryColor, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                )
              )
            )
          )
        ),
        bottomNavigationBar: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          child: Column(
            children: [
            
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                width: MediaQuery.of(context).size.width * 0.8,
                height: 60.0,
                child: RaisedButton(
                  elevation: 0,
                  color: CustomsColors.primaryColor,
                  disabledColor: Colors.grey[500],
                  child: Text('Sair', style: TextStyle(color: Colors.white, fontSize: 17.0),),
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                  onPressed: (){
                    Navigator.push(context, SlideRightRoute(page: HomePage()));
                  }, 
                )
              ),
            ],
          ),
        ),
      )
    );
  }

}
