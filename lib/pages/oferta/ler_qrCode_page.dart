import 'dart:io';
import 'package:afinz/pages/oferta/identificacao_oferta_page.dart';
import 'package:afinz/services/page_transition.dart';
import 'package:afinz/utils/customs_colors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class LerQrCodePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LerQrCodePageState();
}

class _LerQrCodePageState extends State<LerQrCodePage> {
  Barcode result;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');


  @override
  Widget build(BuildContext context) {
    return Scaffold(
    appBar: AppBar(
        elevation: 0,
        backgroundColor: CustomsColors.primaryColor,
        centerTitle: false,
        leading: IconButton(
        icon: Icon(Icons.arrow_back_ios_outlined, color: Colors.white),
        onPressed: (){
            Navigator.pop(context);
        },
        ),
        title: Text('  Leitura de QR Code', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white)),
    ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.085,
              alignment: Alignment.center,
              color: Colors.black,
              width: MediaQuery.of(context).size.width,
              child: Text('Posicione o QR Code para dentro da moldura', style: TextStyle(fontSize: 17.0, color: Colors.yellow, fontWeight: FontWeight.w400),),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.80,
              alignment: Alignment.center,
              color: Colors.black,
              width: MediaQuery.of(context).size.width,
              child: _buildQrView(context),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor: CustomsColors.primaryColor,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: 250),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        result = scanData;
        controller.stopCamera();
        controller.dispose();
        Navigator.push(context, SlideRightRoute(page: IdentificacaoOfertaPage()));
      });
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}