import 'dart:ui';
import 'package:afinz/pages/cadastro/dados_iniciais_1_page.dart';
import 'package:afinz/services/page_transition.dart';
import 'package:afinz/utils/customs_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PoliticaPrivacidadePage extends StatefulWidget {
  @override
  State createState() => new PoliticaPrivacidadePageState();
}

class PoliticaPrivacidadePageState extends State<PoliticaPrivacidadePage> with SingleTickerProviderStateMixin {

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController emailRedefinicaoController = new TextEditingController();
  bool sending = false;
  bool sendingRedefinicao = false;
  bool obscureText = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        Navigator.pop(context);
      },
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: SafeArea(
          child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: SingleChildScrollView( child: Container(
              alignment: Alignment.topCenter,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 60,
                      child: AppBar(
                          elevation: 0,
                          backgroundColor: Colors.transparent,
                          centerTitle: false,
                          leading: IconButton(
                          icon: Icon(Icons.arrow_back_ios_outlined, color: Colors.white),
                          onPressed: (){
                            Navigator.pop(context);
                          },
                          ),
                          title: Text('Politica de privacidade', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white)),
                      ),
                      decoration: BoxDecoration(
                        color: CustomsColors.primaryColor,
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(40)
                        )
                      ),
                    ),

                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.all(20),
                      child: Text(
                        "Politica de privacidade",
                        style: TextStyle(fontSize: 20.0, color: CustomsColors.primaryColor, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),

                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.all(20),
                      child: Text(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui.",
                        style: TextStyle(fontSize: 14.0, color: Colors.black54, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.justify,
                      ),
                    ),

                    
  
                  ],
                )
              )
            )
          )
        ),
        bottomNavigationBar: Container(
          height: MediaQuery.of(context).size.height * 0.24,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                width: MediaQuery.of(context).size.width * 0.8,
                height: 60.0,
                child: RaisedButton(
                  elevation: 0,
                  color: Colors.white,
                  disabledColor: Colors.grey[500],
                  child: Text('Não concordo', style: TextStyle(color: CustomsColors.primaryColor, fontSize: 17.0),),
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0),side: BorderSide(
                    color: CustomsColors.primaryColor
                  )),
                  onPressed: (){
                    Navigator.pop(context);
                  }, 
                )
              ),

            
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                width: MediaQuery.of(context).size.width * 0.8,
                height: 60.0,
                child: RaisedButton(
                  elevation: 0,
                  color: CustomsColors.primaryColor,
                  disabledColor: Colors.grey[500],
                  child: Text('Li e concordo', style: TextStyle(color: Colors.white, fontSize: 17.0),),
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                  onPressed: (){
                    Navigator.push(context, SlideRightRoute(page: DadosIniciais1Page()));
                  }, 
                )
              ),
            ],
          ),
        ),
      )
    );
  }

}
