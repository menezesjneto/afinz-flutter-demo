import 'dart:ui';
import 'package:afinz/models/login_data.dart';
import 'package:afinz/pages/oferta/ler_qrCode_page.dart';
import 'package:afinz/services/page_transition.dart';
import 'package:afinz/utils/customs_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LerOfertaPage extends StatefulWidget {
  @override
  State createState() => new LerOfertaPageState();
}

class LerOfertaPageState extends State<LerOfertaPage> with SingleTickerProviderStateMixin {

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController emailRedefinicaoController = new TextEditingController();
  bool sending = false;
  bool sendingRedefinicao = false;
  bool obscureText = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        Navigator.pop(context);
      },
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: SafeArea(
          child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: SingleChildScrollView( child: Container(
              alignment: Alignment.topCenter,
              margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 20.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    Container(
                      width: 150,
                      height: 150,
                      child: Image.asset("assets/images/logo2.png",fit: BoxFit.contain,),
                    ),

                    Container(
                      margin: EdgeInsets.fromLTRB(40, 0, 40, 20),
                      child: Text(
                        "Está em uma das nossas lojas parceiras? leia o QR code de oferta e peça seu cartão.",
                        style: TextStyle(fontSize: 17.0, color: Colors.black87, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
              
                    Container(
                      margin: EdgeInsets.all(20.0),
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: 60.0,
                      child: RaisedButton(
                        color: CustomsColors.primaryColor,
                        disabledColor: Colors.grey[500],
                        child: Text('Ler QR Code', style: TextStyle(color: Colors.white, fontSize: 17.0),),
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                        onPressed: (){
                          Navigator.push(context, SlideRightRoute(page: LerQrCodePage()));
                        }, 
                      )
                    ),

                    Container(
                      margin: EdgeInsets.only(top: 20.0, bottom: 20),
                      child: Text(
                        "ou",
                        style: TextStyle(fontSize: 17.0, color: Colors.black87, fontWeight: FontWeight.bold),
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.all(20.0),
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: 60.0,
                      child: RaisedButton(
                        color: CustomsColors.primaryColor,
                        disabledColor: Colors.grey[500],
                        child: Text('Solicite seu cartao aqui', style: TextStyle(color: Colors.white, fontSize: 17.0),),
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                        onPressed: (){

                        }, 
                      )
                    ),
                        
                  ],
                )
              )
            )
          )
        ),
        bottomNavigationBar: Container(height: 0.0),
      )
    );
  }

}
