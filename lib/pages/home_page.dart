import 'dart:ui';

import 'package:afinz/utils/customs_colors.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  List<dynamic> imagesBanners = [
    Image.network(
        'https://img.lacadordeofertas.com.br/site/MTcwMjVfL3RtcC9waHB3QUpkWW5fMTYxNDg2NjQ4OA==.png'),
    Image.network(
        'https://img.lacadordeofertas.com.br/site/MTcwMjVfL3RtcC9waHA3TXR2SmhfMTU5NzkzMjE1MA==.JPG'),
    Image.network(
        'https://img.lacadordeofertas.com.br/site/MTcwMjVfL3RtcC9waHBiZVFBU05fMTU5ODkwMDAwMQ==.png'),
  ];
  List<Map<dynamic, dynamic>> _views = [
    {"title": "Cartão online", "img": 'assets/images/cartao.png', "page": '/'},
    {"title": "Faturas", "img": "assets/images/faturas.png", "page": '/'},
    {"title": "Pagar conta", "img": "assets/images/pagar.png", "page": '/'},
    {
      "title": "Comprovantes",
      "img": "assets/images/comprovante.png",
      "page": '/'
    },
    {"title": "Senha do cartão", "img": "assets/images/senha.png", "page": '/'},
    {"title": "Recarga", "img": "assets/images/recarga.png", "page": '/'},
  ];
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Center(
            child: Stack(
              children: [
                Container(
                  child: Column(children: [
                    Container(
                      height: screenHeight / 3,
                      width: screenWidth,
                      color: CustomsColors.primaryColor,
                    ),
                  ]),
                ),
                Positioned(
                  child: Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.only(left: 20, top: 60),
                          child: Text(
                            'Bom dia, José Neto!',
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.bold),
                          ),
                        ),
                        carousel(context),
                        Container(
                          margin: EdgeInsets.only(left: 50, right: 50),
                          child: PhysicalModel(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            elevation: 5,
                            color: Colors.transparent,
                              child: Stack(
                              children: [
                                Container(
                                  height: 120,
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(5),
                                  child: Column(
                                    children: [ 
                                      Icon(FontAwesomeIcons.lock, color: Colors.grey[600], size: 30),
                                      Container(
                                        margin: EdgeInsets.only(top: 7),
                                        alignment: Alignment.center,
                                        child: Text(
                                          'Cartão físico Bloqueado',
                                          style: TextStyle(
                                              color: Colors.grey[600], fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 7, bottom: 3),
                                        alignment: Alignment.center,
                                        child: Text(
                                          'xxxx xxxx xxxx 0888',
                                          style: TextStyle(
                                              color: Colors.grey[600], fontWeight: FontWeight.w500),
                                        ),
                                      ),

                                      Container(
                                        margin: EdgeInsets.all(5),
                                        width: MediaQuery.of(context).size.width * 0.8,
                                        height: 20.0,
                                        child: RaisedButton(
                                          elevation: 0,
                                          color: CustomsColors.primaryColor,
                                          disabledColor: Colors.grey[500],
                                          child: Text('Desbloquear cartão', style: TextStyle(color: Colors.white, fontSize: 12.0),),
                                          shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(3.0)),
                                          onPressed: (){
                                            Navigator.pop(context);
                                          }, 
                                        )
                                      ),
                                    ],
                                  ),
                                  decoration: new BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                    gradient: new LinearGradient(
                                        colors: [
                                          Colors.grey[300],
                                          Colors.grey[200],
                                        ],
                                        begin: FractionalOffset(0.0, 0.0),
                                        end: FractionalOffset(1.0, 0.0),
                                        stops: [0.0, 1.0],
                                        tileMode: TileMode.clamp
                                    ),
                                  ),
                                ),
                                Positioned(
                                  right: 20,
                                  top: 10,
                                  child: Icon(FontAwesomeIcons.ccVisa, color: Colors.grey[400], size: 30)
                                ),
                              ],
                            ),
                          )
                        ),

                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(top: 20),
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: 10,
                                width: 20,
                                decoration: new BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: CustomsColors.primaryColor
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                height: 10,
                                width: 10,
                                decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.grey[400]
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                height: 10,
                                width: 10,
                                decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.grey[400]
                                ),
                              ),
                            ],
                          ),
                      ),

                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                child: Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Limite utilizado',
                                        style: TextStyle(
                                            color: Colors.black87, fontWeight: FontWeight.w500),
                                      ),
                                    ),

                                    Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        r'R$ 380,00',
                                        style: TextStyle(
                                            color: CustomsColors.primaryColor, fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 50,
                                child: VerticalDivider(thickness: 1)
                              ),

                              Container(
                                child: Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Limite utilizado',
                                        style: TextStyle(
                                            color: Colors.black87, fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    Container(
                                      alignment: Alignment.center,
                                      child: Row(
                                        children: [
                                          Text(
                                            r'R$ 1200,00',
                                            style: TextStyle(
                                                color: CustomsColors.primaryColor, fontWeight: FontWeight.bold),
                                          ),
                                          VerticalDivider(color: Colors.transparent, width: 5),
                                          Icon(Icons.remove_red_eye, color: CustomsColors.primaryColor, size: 15)
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),


                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'Fatura Fechada',
                                  style: TextStyle(
                                      color: Colors.black87, fontWeight: FontWeight.w500),
                                ),
                              ),

                              Divider(color: Colors.transparent, height: 5),
                              
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Row(
                                  children: [
                                    Text(
                                      r'R$ 1000,00',
                                      style: TextStyle(color: CustomsColors.primaryColor, fontWeight: FontWeight.bold, fontSize: 22),
                                    ),
                                    Spacer(),
                                    Icon(Icons.keyboard_arrow_right_outlined, color: CustomsColors.primaryColor),
                                  ],
                                ),
                              ),

                              Divider(color: Colors.transparent, height: 5),

                              Container(
                                alignment: Alignment.centerLeft,
                                child: Row(
                                  children: [
                                    Text(
                                      'Vencimento',
                                      style: TextStyle(
                                          color: Colors.black87, fontWeight: FontWeight.w400),
                                    ),
                                    VerticalDivider(color: Colors.transparent, width: 5),
                                    Text(
                                      '02/07/2021',
                                      style: TextStyle(
                                          color: Colors.black87, fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                              ),
                              
                            ],
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey[400]),
                            borderRadius: BorderRadius.all(Radius.circular(5))
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.4,
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(
                              left: 20, right: 20, bottom: 20),
                          child: _createGrid(context, _views),
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ));
  }


  Widget carousel(context) {
    return Container(
        height: 250.0,
        margin: EdgeInsets.only(top: 50),
        padding: EdgeInsets.only(bottom: 20),
        width: MediaQuery.of(context).size.width,
        child: Carousel(
          autoplayDuration: Duration(seconds: 10),
          dotIncreasedColor: Colors.transparent,
          images: [
            Container(
              margin: EdgeInsets.only(left: 30, right: 30),
              child: Stack(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    width: 400,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 15, left: 20),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Cartão online',
                            style: TextStyle(
                                color: Colors.black87, fontWeight: FontWeight.w500, fontSize: 28),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 25, left: 20),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'xxxx xxxx xxxx 0888',
                            style: TextStyle(
                                color: Colors.black87, fontWeight: FontWeight.w500, fontSize: 24),
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(left: 20, top: 10),
                          child: Row(
                            children: [
                              Container(
                                child: Column(
                                  children: [
                                    Text(
                                      'Validade',
                                      style: TextStyle(
                                      color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 17),
                                    ),
                                    Text(
                                      '07 / 19',
                                      style: TextStyle(
                                      color: Colors.black87, fontWeight: FontWeight.w500, fontSize: 17),
                                    ),
                                  ],
                                ),
                              ),
                              VerticalDivider(color: Colors.transparent),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Código de segurança',
                                      style: TextStyle(
                                      color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 17),
                                    ),
                                    Text(
                                      '333',
                                      style: TextStyle(
                                      color: Colors.black87, fontWeight: FontWeight.w500, fontSize: 17),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(top: 15, left: 20),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Heitor R da Silva',
                            style: TextStyle(
                                color: Colors.black87, fontWeight: FontWeight.w500, fontSize: 24),
                          ),
                        ),

                      ],
                    ),
                    decoration: BoxDecoration(
                      gradient: new LinearGradient(
                          colors: [
                            Colors.grey[100],
                            Colors.grey[300],
                          ],
                          begin: FractionalOffset(0.0, 0.0),
                          end: FractionalOffset(1.0, 0.0),
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      color: Colors.yellow,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 15.0,
                          spreadRadius: 0.0,
                          offset: Offset(0.0, 0.0),
                        ),
                      ]
                    ),
                  ),
                  Positioned(
                    right: 20,
                    top: 10,
                    child: Icon(FontAwesomeIcons.ccVisa, color: Colors.black54, size: 40)
                  ),
                ],
              ),
            ),
          ],
          radius: Radius.circular(10),
          dotSize: 8.0,
          autoplay: true,
          dotSpacing: 15.0,
          boxFit: BoxFit.cover,
          dotColor: Colors.transparent,
          indicatorBgPadding: 15.0,
          dotBgColor: Colors.transparent,
          borderRadius: true,
        ));
  }


  Widget _createGrid(context, _views) {
    return GridView.builder(
        shrinkWrap: true,
        primary: false,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10.0,
            mainAxisSpacing: 10.0,
            childAspectRatio: 2),
        itemCount: _views.length,
        itemBuilder: (context, index) {
          return AnimationConfiguration.staggeredGrid(
              position: index,
              duration: Duration(milliseconds: 1500),
              columnCount: 4,
              delay: Duration(milliseconds: 900),
              child: SlideAnimation(
                duration: Duration(milliseconds: 900),
                child: FadeInAnimation(
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Image.asset(
                            _views[index]['img'],
                            fit: BoxFit.contain,
                            height: 40,
                            width: 40,
                          ),
                        ),
                        Divider(color: Colors.transparent, height: 5),
                        Text(
                        _views[index]['title'],
                        style: TextStyle(
                            color: Colors.black87, fontWeight: FontWeight.w400),
                      )
                      ],
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey[400]),
                      borderRadius: BorderRadius.all(Radius.circular(5))
                    ),
                  ),
                      
              )
            )
          );
        });
  }
}
