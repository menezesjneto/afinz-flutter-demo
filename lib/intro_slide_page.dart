import 'dart:io';
import 'package:afinz/pages/login/login_page.dart';
import 'package:afinz/utils/customs_colors.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:lottie/lottie.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
class IntroSlidePage extends StatefulWidget {
  @override
  _IntroSlidePageState createState() => _IntroSlidePageState();
}

class _IntroSlidePageState extends State<IntroSlidePage> {
  final introKey = GlobalKey<IntroductionScreenState>();


  @override
  void initState() {
    super.initState();
  }

  void onDonePress() async {
    SharedPreferences setPage = await SharedPreferences.getInstance();
       setPage.setString('introPageExibida', 'introPageExibida');
     Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (BuildContext context) => LoginPage())); 
  }



  @override
  Widget build(BuildContext context) {
    const pageDecorationFirst = PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: TextStyle(fontSize: 19.0),
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Color(0xff00C6CC),
      imagePadding: EdgeInsets.only(top: 50),
      bodyAlignment: Alignment.topCenter,
      imageAlignment: Alignment.center,
      imageFlex: 1,
      bodyFlex: 1,
    );

    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: TextStyle(fontSize: 19.0),
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Color(0xff00C6CC),
      bodyAlignment: Alignment.topCenter,
      imageAlignment: Alignment.center,
      imageFlex: 1,
      bodyFlex: 0,
      imagePadding: EdgeInsets.only(top: 50),
      footerPadding: EdgeInsets.only(bottom: 50),
    );

    return new Material(
      child: IntroductionScreen(
        key: introKey,
        globalBackgroundColor: CustomsColors.primaryColor,
        pages: [
          PageViewModel(
            titleWidget: 
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: AnimatedTextKit(
                animatedTexts: [
                  ColorizeAnimatedText(
                    'Seja bem vindo à plataforma\nAfinz!',
                    textStyle: TextStyle(fontSize: 25.0, color: Colors.white, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                    colors: [
                      Colors.white,
                      Colors.white54,
                      Colors.yellow[100],
                    ],
                  ),
                ],
                isRepeatingAnimation: false,
                totalRepeatCount: 3,
                onTap: () {
                  
                },
              ),
            ),
            bodyWidget: Text("", 
              style: TextStyle(fontSize: 22.0, color: Colors.black87, fontWeight: FontWeight.w300), 
              textAlign: TextAlign.center),
            image: Column(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: SafeArea(
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white
                      ),
                      padding: EdgeInsets.only(top: 5),
                      margin: const EdgeInsets.only(top: 16, right: 16),
                      child: Image.asset('assets/images/logo1.png', width: 130, fit: BoxFit.contain,),
                    ),
                  ),
                ),
                Container(),
              ],
            ),
            decoration: pageDecorationFirst,
          ),
          PageViewModel(
            image: Lottie.asset('assets/jsons/local1.json', height: 250, fit: BoxFit.cover),
            title: 'Localização',
            bodyWidget: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: AnimatedTextKit(
                animatedTexts: [
                  ColorizeAnimatedText(
                    'É necessário a liberação do acesso ao GPS para obter dados de geolocalização',
                    textStyle: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                    colors: [
                      Colors.white,
                      Colors.white,
                      Colors.white60,
                    ],
                  ),
                ],
                isRepeatingAnimation: false,
                  totalRepeatCount: 1,
                  onTap: () {},
              )),
            footer: Container(
              margin: EdgeInsets.all(20.0),
              height: 60.0,
              child: RaisedButton(
                elevation: 0,
                color: CustomsColors.secundaryColor,
                splashColor: Colors.blue,
                disabledColor: Colors.grey[200],
                child: Text('Conceder Permissão', style: TextStyle(color: Colors.white, fontSize: 17.0),),
                shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                onPressed: () async {
                  await Permission.location.request();
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (BuildContext context) => LoginPage())); 
                },
              )
            ),
            decoration: pageDecoration,
          ),
        ],
        onDone: () async{
          SharedPreferences setPage = await SharedPreferences.getInstance();
           // setPage.setString('introPageExibida', 'introPageExibida');
          Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (BuildContext context) => LoginPage())); 
        },
        //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
        showSkipButton: false,
        showNextButton: true,
        skipFlex: 0,
        nextFlex: 0,
        //rtl: true, // Display as right-to-left
        skip: const Text('Próximo', style: TextStyle(fontWeight: FontWeight.w600, color: Colors.white)),
        next: const Text('Próximo', style: TextStyle(fontWeight: FontWeight.w600, color: Colors.white)),
        done: const Text('Pronto', style: TextStyle(fontWeight: FontWeight.w600, color: Colors.white)),
        curve: Curves.fastLinearToSlowEaseIn,
        controlsMargin: const EdgeInsets.all(16),
        controlsPadding: const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
        dotsDecorator: const DotsDecorator(
          size: Size(10.0, 10.0),
          color: Color(0xFFBDBDBD),
          activeSize: Size(22.0, 10.0),
          activeColor: Colors.white,
          activeShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)),
          ),
        ),
        dotsContainerDecorator: const ShapeDecoration(
          color: Colors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
        ),
      ),
    );
  }

}
