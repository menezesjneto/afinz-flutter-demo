import 'package:flutter/material.dart';

class MainDataProvider with ChangeNotifier {
  List<String> occupations = [];
  List<String> categories = [];
  int pageIndex = 0;
  int initialIndexTab = 0;
  int uid;
  int notificationCount = 0;
  String photoProfile = ';';

  bool exibirRedesSociais = false;

  setNotificationCount(int qtd){
    notificationCount = qtd;
    notifyListeners();
  }

  setPhotoProfile(String url){
    photoProfile = url;
    notifyListeners();
  }

  changePageIndex(int id) {
    pageIndex = id;
    notifyListeners();
  }

  changeExibirRedesSociais(bool b) {
    exibirRedesSociais = b;
    notifyListeners();
  }

  changeInitialTabIndex(int id) {
    initialIndexTab = id;
    notifyListeners();
  }

  resetPageIndex(){
    pageIndex = 0;
    initialIndexTab = 0;
  }


  pageIndexBack() {
    pageIndex -= 1;
    notifyListeners();
  }
}
