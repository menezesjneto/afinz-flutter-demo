// @dart=2.9
import 'package:afinz/intro_slide_page.dart';
import 'package:afinz/pages/home_page.dart';
import 'package:afinz/services/main_data_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
    .then((_) async {
      initializeDateFormatting().then((_) => runApp(MyApp()));
    });
}

class MyApp extends StatelessWidget {

  Future<Widget> choosePage() async{
     SharedPreferences pages = await SharedPreferences.getInstance();
     bool introPage = pages.containsKey('introPageExibida');
     if(introPage){
       bool usuarioLogado = pages.containsKey('user');
       if(usuarioLogado){
         return IntroSlidePage(); //home
       }else{
         return IntroSlidePage();
       }
     }else{
       return IntroSlidePage();
     }
  }

  @override
  Widget build(BuildContext context) {
    return  MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => MainDataProvider()),
      ],
      child: MaterialApp(
      title: "tradeFISH",
      theme: ThemeData(
        primaryColor: Colors.black,
      ),
      home: FutureBuilder<Widget>(
        future: choosePage(),
        builder: (BuildContext context, AsyncSnapshot<Widget> snapshot){
          if(snapshot.hasData) return snapshot.data;
          else return Container();
        }
      ),
      debugShowCheckedModeBanner: false));
  }
}

