class LoginData {

  String email;
  String senha;

  LoginData({
    this.email,
    this.senha
  });

  factory LoginData.fromMap(Map<String, dynamic> json) => new LoginData(
    email: json["email"],
    senha: json["senha"]
  );

  factory LoginData.fromJson(Map<String, dynamic> parsedJson){
    return LoginData(
      email: parsedJson["email"],
      senha: parsedJson["senha"]
    );
  }

  Map<String, dynamic> toJson() => {
    "email": email,
    "password": senha,
  };

}