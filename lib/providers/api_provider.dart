import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:afinz/utils/app_exception.dart';
import 'package:http/http.dart' as http;



class ApiProvider {

  static final ApiProvider _singleton = new ApiProvider._internal();
  factory ApiProvider() {
    return _singleton;
  }

  ApiProvider._internal();

  static final Map<String, String> HEADER_APPLICATION_JSON = {
    'accept': 'application/json', 
    'content-type': 'application/json',
  };

  static final Map<String, String> HEADER_APPLICATION_WWW_FORM = {
    'accept': 'x-www-form-urlencoded', 
    'content-type': 'x-www-form-urlencoded'
  };

  static final Map<String, String> HEADER_MULTIPART_FORMDATA = {
    'accept': '*/*', 
    'content-type': 'multipart/form-data'
  };
  static final String urlServer = 'pacific-falls-49086.herokuapp.com';

  static bool validEmail(text){
    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return regex.hasMatch(text);
  }

  static Future<Map<String, String>> _getHeader(String headerType, String token) async {
    Map<String, String> header = Map<String, String>();

    if(headerType == 'form') header = HEADER_APPLICATION_WWW_FORM;
    else if(headerType == 'multipart') header = HEADER_MULTIPART_FORMDATA;
    else if(headerType == 'json') header = HEADER_APPLICATION_JSON;

    if(token != null) header['Authorization'] = token;

    return header;
  }

  static double getSizeImageMegaBytes(File image){
    if(image != null){
      int bytes = image.readAsBytesSync().lengthInBytes;
      double kb = bytes / 1024; //KB
      double mb = kb / 1024;  //MB
      
      print('tamanho imagem KB: ' + kb.toString());
      print('tamanho imagem MB: ' + mb.toString());
      return mb;
    }else{
      return 0;
    }
  }

  static Future<dynamic> restGet(String path, Map<String, dynamic> queryParams, String headerType, String token) async {
    try {
      var authority = '';
      var api = '/api/';

      authority = ApiProvider.urlServer;
      

      Uri uri = new Uri.https(authority, api + path, queryParams);
      
      var header = await _getHeader(headerType, token);

      final response =  await http.get(
        uri, 
        headers: header
      );

      return _returnResponse(response);
    } catch(e) {
      return e;
    }

  }
  static Future<dynamic> restPost(String path, Map payload, Map<String, dynamic> queryParams, String headerType, String token) async {
    try {
      var authority = '';
      var api = '/api/';

      authority = ApiProvider.urlServer;

      Uri uri = new Uri.https(authority, api + path, queryParams);

      var header = await _getHeader(headerType, token);

      var body = _parseBody(payload, headerType);

      final response = await http.post(
        uri,
        headers: header,
        body: body.toString()
      );

      return _returnResponse(response);
    } catch(e) {
      return e;
    }
  }

  static dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        if(response.body == "") return null;
        else {
          try {
            var responseJson = json.decode(utf8.decode(response.bodyBytes));
            return responseJson;
          } catch (e) {
            return response.body;
          }
        }
        
        return null;
      case 201:
        if(response.body == "") return null;
        else {
          try {
            var responseJson = json.decode(utf8.decode(response.bodyBytes));
            return responseJson;
          } catch (e) {
            return response.body;
          }
        }

        return null;
      case 204:
        return null;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
        throw UnauthorisedException(response.body.toString());
       case 403:
         throw ForbbidenException(response.body.toString());
      case 404:
        throw NotFoundException(response.body.toString());
       case 500:
         throw InternalServerError(response.body.toString());
      default:
        throw FetchDataException('Ocorreu um erro de comunicação com o servidor. Código do Status : ${response.statusCode}');
    }
  }

  static dynamic _parseBody(dynamic payload, String headerType) {
    var body;

    if(headerType == 'form' && payload != null) body = payload;
    else if(headerType == 'multipart' && payload != null) body = payload;
    else if(headerType == 'json' && payload != null) body = json.encode(payload);
    else body = payload;

    return body;
  }




}